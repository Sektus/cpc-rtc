TARGETS=prog

ifneq ($(wildcard /opt/cuda/.),)
	CUDA_PATH=/opt/cuda
else
	CUDA_PATH=/usr/local/cuda
endif

SAMPLES_DIR=$(CUDA_PATH)/samples
NVCC=$(CUDA_PATH)/bin/nvcc

SRCDIR=./cpc

CXX=g++
CXXFLAGS= -std=c++1y -Ofast -funroll-loops -fopenmp -m64 -march=native -mavx2 -mfma -ffast-math -I. -I$(CUDA_PATH)/include
LDFLAGS=-lnvrtc -lcuda -lmvec -lm -L$(CUDA_PATH)/lib64 -lgomp

HEADS=$(shell find $(SRCDIR) -type f -name "*.h")

all: best

best:
	$(CXX) $(CXXFLAGS) main.cpp -o prog $(LDFLAGS)

default:
	$(CXX) $(CXXFLAGS) main.cpp -o prog $(LDFLAGS) -DDEFAULT

noinline:
	$(CXX) $(CXXFLAGS) main.cpp -o prog $(LDFLAGS) -DNOINLINE

nounroll:
	$(CXX) $(CXXFLAGS) main.cpp -o prog $(LDFLAGS) -DNOUNROLL

divergence:
	$(CXX) $(CXXFLAGS) main.cpp -o prog $(LDFLAGS) -DDIVERGENCE

noinline_nounroll:
	$(CXX) $(CXXFLAGS) main.cpp -o prog $(LDFLAGS) -DNOINLINE -DNOUNROLL

noinline_divergence:
	$(CXX) $(CXXFLAGS) main.cpp -o prog $(LDFLAGS) -DNOINLINE -DDIVERGENCE

nounroll_divergence:
	$(CXX) $(CXXFLAGS) main.cpp -o prog $(LDFLAGS) -DNOUNROLL -DDIVERGENCE

noinline_nounroll_divergence:
	$(CXX) $(CXXFLAGS) main.cpp -o prog $(LDFLAGS) -DNOINLINE -DNOUNROLL -DDIVERGENCE

deviceQuery.cpp: $(SAMPLES_DIR)/1_Utilities/deviceQuery/deviceQuery.cpp
	cp $< .

device_query: deviceQuery.cpp
	$(NVCC) -I$(SAMPLES_DIR)/common/inc -m64 $< -o $@

clean: 
	$(RM) $(TARGETS)

.PHONY: all clean best default noinline nounroll divergence noinline_nounroll noinline_divergence nouroll_divergence noinline_nounroll_divergence
