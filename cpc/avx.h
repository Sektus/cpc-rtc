#ifndef CPC_AVX_H
#define CPC_AVX_H

#include <immintrin.h>
#include <stdio.h>

inline void _mm256_print_epi32(__m256i x){
    int vec_x[8];
    _mm256_storeu_si256(reinterpret_cast<__m256i *>(vec_x),x);
    printf("%d %d %d %d %d %d %d %d \n", vec_x[7], vec_x[6], vec_x[5], vec_x[4],
                     vec_x[3], vec_x[2], vec_x[1], vec_x[0]);
}

inline void _mm256_print_ps(__m256 x){
    float vec_x[8];
    _mm256_storeu_ps(vec_x,x);
    printf("%12.8f %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f \n", vec_x[7], vec_x[6], vec_x[5], vec_x[4],
                     vec_x[3], vec_x[2], vec_x[1], vec_x[0]);
}

inline __m256 _mm256_cvt_epu32_ps(const __m256i v){
  const __m256i mask_lo = _mm256_set1_epi32(0xFFFF);
  const __m256  f65536  = _mm256_set1_ps(65536.0f);

  __m256i v_lo  = _mm256_and_si256(v, mask_lo);
  __m256i v_hi  = _mm256_srli_epi32(v, 16);
  __m256  v_lof = _mm256_cvtepi32_ps(v_lo); 
  __m256  v_hif = _mm256_cvtepi32_ps(v_hi); 
          v_hif = _mm256_mul_ps(f65536, v_hif);
  return          _mm256_add_ps(v_hif, v_lof);
}

inline __m256 _mm256_fcvt_epu32_ps(const __m256i v)
{
	__m256i v2 = _mm256_srli_epi32(v, 1);
	__m256i v1 = _mm256_and_si256(v, _mm256_set1_epi32(1));
	__m256 v2f = _mm256_cvtepi32_ps(v2);
	__m256 v1f = _mm256_cvtepi32_ps(v1);
	return _mm256_add_ps(_mm256_add_ps(v2f, v2f), v1f);
}

#endif //CPC_AVX_H
