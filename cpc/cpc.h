#ifndef CPC_CPC_H
#define CPC_CPC_H

#include <cpc/cuda/cuda.h>
#include <cpc/avx.h>
#include <cpc/cpu.h>
#include <cpc/gpu.h>
#include <cpc/host.h>
#include <cpc/math.h>
#include <cpc/model.h>
#include <cpc/options.h>
#include <cpc/random.h>

#endif //CPC_CPC_H
