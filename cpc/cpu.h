#ifndef CPC_CPU_H
#define CPC_CPU_H

#include <thread>
#include <stdlib.h>

#include <cpc/avx.h>
#include <cpc/gpu.h>
#include <cpc/math.h>
#include <cpc/model.h>
#include <cpc/host.h>
#include <cpc/options.h>
#include <cpc/random.h>

namespace cpc {

template<int NPATHS>
inline void cpu_kernel(float *dx, unsigned int seed, const model m, const kernel params){
  unsigned int pcd_seed = seed;
  int pcd  = m.get_pcd(pcd_seed); 

  __m256i x[NPATHS], x0[NPATHS];
  __m256i seeds[NPATHS];
  __m256  xfc = _mm256_setzero_ps();
  
  for (int i = 0; i < NPATHS; ++i){
    unsigned int s[8] = {++seed, ++seed, ++seed, ++seed, ++seed, ++seed, ++seed, ++seed};
    seeds[i]   = _mm256_loadu_si256(reinterpret_cast<__m256i *>(&s)); 
    __m256 uni = _mm256_fmsub_ps(cpc::uniform(seeds[i]), _mm256_set1_ps(2.0f), _mm256_set1_ps(1.0f));
    x[i]       = _mm256_cvtps_epi32(_mm256_mul_ps(uni, m.base));
  }

  int step = params.trigger;
  while (step != 0){
    pcd = min(pcd, step);
    step -= pcd;

    for (; pcd > 0; --pcd){
      for (int path = 0; path < NPATHS; ++path){
        m.predcorr(x[path]);
      }
    }

    for (int path = 0; path < NPATHS; ++path){
      x[path] = _mm256_add_epi32(x[path], m.get_jump(seeds[path]));
      cpc::fold(x[path], xfc);
    }
    pcd = m.get_pcd(pcd_seed);
  }
  
  xfc = _mm256_setzero_ps();
  for (int path = 0; path < NPATHS; ++path){
    x0[path] = x[path];
  }

  step = params.steps;
  while (step != 0){
    pcd = min(pcd, step);
    step -= pcd;

    for (; pcd > 0; --pcd){
      for (int path = 0; path < NPATHS; ++path){
        m.predcorr(x[path]);
      }
    }

    for (int path = 0; path < NPATHS; ++path){
      x[path] = _mm256_add_epi32(x[path], m.get_jump(seeds[path]));
      cpc::fold(x[path], xfc);
    }
    pcd = m.get_pcd(pcd_seed);
  }

  __m256 b = _mm256_set1_ps(powf(2.0f, -22));
  __m256 sum = _mm256_setzero_ps();
  for (int path = 0; path < NPATHS; ++path){
    sum = _mm256_add_ps(sum, _mm256_mul_ps(b, _mm256_cvtepi32_ps(_mm256_sub_epi32(x[path], x0[path]))));
  }
  b = _mm256_set1_ps(2.0f / NPATHS);
  sum = _mm256_add_ps(sum, _mm256_mul_ps(b, xfc));

  _mm256_store_ps(dx, sum);   
}

struct cpu {
  cpc::options *opt;

  size_t size;
  float *x;

  int threads;
  int num_runs;
  int num_kernels;
  
  inline cpu(cpc::options &opt): opt(&opt) {
    num_runs = 1;
    num_kernels = 1;

    align_paths();

    size = 8 * num_kernels * threads * sizeof(float);
    x = (float *) aligned_alloc(32, size);
  }

  inline cpu(gpu &g): opt(g.opt){
    num_runs = g.num_runs;
    num_kernels = g.num_kernels;

    align_paths();
    
    size = 2 * 8 * num_kernels * threads * sizeof(float);
    x = (float *) aligned_alloc(32, size);
  }

  inline ~cpu(){
    free(x);
  }

  void align_paths(){
    int cores = std::thread::hardware_concurrency();

    if (opt->conf != cpc::configuration::HYBRID){ 
      int align = cores * opt->npaths * 8;
      opt->paths = ((int)(opt->paths + align - 1)) / align * align;
      threads = opt->paths / (8 * opt->npaths * num_runs);
    } else {
      int align = cores * opt->cpu_npaths * 8;
      opt->cpu_paths = ((int)(opt->paths * opt->cpu + align - 1)) / align * align;
      threads = opt->cpu_paths / (8 * opt->cpu_npaths * num_runs);
    }
  }

  void run(int model_idx, int npaths, int idx = 0){
    for (int j = 0; j < num_kernels; ++j){
      const cpc::model m = opt->d[model_idx + j];
      const cpc::kernel params = opt->params;
      const int stride = (idx * num_kernels + j) * threads;
      #pragma omp parallel for firstprivate(m, params)
      for (int i = 0; i < threads; ++i){
        int seed = time(NULL) + 8 * i * npaths;
        if (npaths == 1){
          cpc::cpu_kernel<1>(&x[(stride + i) * 8], seed, m, params);
        } else if (npaths == 2 || npaths == 3){
          cpc::cpu_kernel<2>(&x[(stride + i) * 8], seed, m, params);
        } else {
          cpc::cpu_kernel<4>(&x[(stride + i) * 8], seed, m, params);
        }
      }
    }
  }

  float sum(int idx = 0){
    return cpc::sum(&x[idx * threads * 8], threads * 8);
  }
};
  
} //namespace cpc

#endif //CPC_CPU_H
