#ifndef CPC_CUDA_CUDA_H
#define CPC_CUDA_CUDA_H

#include <cuda.h>

#include <cpc/cuda/context.h>
#include <cpc/cuda/device.h>
#include <cpc/cuda/dim.h>
#include <cpc/cuda/errchk.h>
#include <cpc/cuda/function.h>
#include <cpc/cuda/memory.h>
#include <cpc/cuda/module.h>
#include <cpc/cuda/nvrtc.h>
#include <cpc/cuda/stream.h>

namespace cpc { namespace cuda {

inline void init(){
  cuErrchk(cuInit(0));
}

inline int get_driver_version(){
  int res;
  cuErrchk(cuDriverGetVersion(&res));
  return res;
}

inline const char *get_std(){
  int version = cpc::cuda::get_driver_version();
  if (version <= 6050){
    return "-std=c++03";
  } else if (version <= 8000){
    return "-std=c++11";
  } else if (version <= 10020){
    return "-std=c++14";
  } else {
    return "-std=c++17";
  }
} 

inline int get_max_blocks_per_multiprocessor(int version){
  if ((version < 5000) || (version == 7050)){
    return 16;
  } else {
    return 32;
  }
}

inline int get_max_concurrent_kernels(int version){
  if ((version >= 2000 && version <= 3000) || version == 5030 || version == 6020 || version == 7020){
    return 16;
  } else if (version == 3020){
    return 4;
  } else if ((version >= 3050 && version <= 5020) || version == 6010){
    return 32;
  } else {
    return 128;
  }
}


} //namespace cuda
} //namespace cpc

#endif //CPC_CUDA_CUDA_H
