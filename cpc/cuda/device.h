#ifndef CPC_CUDA_DEVICE_H
#define CPC_CUDA_DEVICE_H

#include <cuda.h>
#include <string>

#include <cpc/cuda/errchk.h>

namespace cpc { namespace cuda {

class device{
  private:
    CUdevice dev;
  public:
    inline device(int ordinal){
      cuErrchk(cuDeviceGet(&dev, ordinal));
    }

    inline operator CUdevice() const { return dev; }

    int get_attribute(CUdevice_attribute attrib) const{
      int attr;
      cuErrchk(cuDeviceGetAttribute(&attr, attrib, dev));
      return attr;
    }

    int get_version(){
      int major = get_attribute(CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MAJOR);
      int minor = get_attribute(CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MINOR);
      return major * 1000 + minor * 10;
    }

    const char *get_arch() const {
      int major = get_attribute(CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MAJOR);
      int minor = get_attribute(CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MINOR);

      if (major == 1){
        if (minor == 0){
          return "-arch=compute_10";
        } else if (minor == 1){
          return "-arch=compute_11";
        } else if (minor == 2){
          return "-arch=compute_12";
        } else {
          return "-arch=compute_13";
        }
      } else if (major == 2){
        if (minor == 0) {
          return "-arch=compute_20";
        } else {
          return "-arch=compute_21";
        }
      } else if (major == 3){
        if (minor == 0){
          return "-arch=compute_30";
        } else if (minor == 2){
          return "-arch=compute_32";
        } else if (minor == 5){
          return "-arch=compute_35";
        } else {
          return "-arch=compute_37";
        }
      } else if (major == 5){
        if (minor == 0){
          return "-arch=compute_50";
        } else if (minor == 2){
          return "-arch=compute_52";
        } else {
          return "-arch=compute_53";
        }
      } else if (major == 6){
        if (minor == 0) { 
          return "-arch=compute_60";
        } else if (minor == 1){
          return "-arch=compute_61";
        } else {
          return "-arch=compute_62";
        }
      } else if (major == 7){
        if (minor == 0){
          return "-arch=compute_70";
        } else if (minor == 2){
          return "-arch=compute_72"; 
        } else {
          return "-arch=compute_75";
        }
      } else {
        if (minor == 0){
          return "-arch=compute_80";
        } else {
          return "-arch=compute_86";
        }
      }
    }
};


} //namespace cuda
} //namespace cpc

#endif //CPC_CUDA_DEVICE_H
