#ifndef CPC_CUDA_DIM_H
#define CPC_CUDA_DIM_H

namespace cpc { namespace cuda {

class dim3 {
  public:
    int x;
    int y;
    int z;

    inline constexpr dim3(int x=1, int y=1, int z=1): x(x), y(y), z(z){}
};

} //namespace cuda
} //namespace cpc

#endif //CPC_CUDA_DIM_H
