#ifndef CPC_DEFINES_H
#define CPC_DEFINES_H

#if defined(NOUNROLL)
#define __unroll__ _Pragma("unroll (1)")
#elif defined(DEFAULT)
#define __unroll__
#else
#define __unroll__ _Pragma("unroll (NPATHS)")
#endif

#define CONCAT_(A, B) A ## B
#define CONCAT(A, B) CONCAT_(A, B)

#endif
