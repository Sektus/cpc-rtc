#ifndef CPC_GPU_H
#define CPC_GPU_H

#include <nvrtc.h> 
#include <cuda.h>
#include <time.h>

#include <cpc/cuda/cuda.h>
#include <cpc/model.h>
#include <cpc/host.h>
#include <cpc/options.h>

namespace cpc {

struct gpu {
  const char *version;
  const char *standard;

  cpc::options *opt;
  cpc::cuda::device dev;
  cpc::cuda::primary_context ctx;
  
  cpc::cuda::module moments;
  cpc::cuda::dim3 blocks;
  cpc::cuda::dim3 threads;  
  
  int num_kernels;
  int num_runs;

  size_t size;
  int stride;
  cpc::cuda::device_ptr<float> d_x;
  cpc::cuda::host_ptr<float> h_x;

  cpc::cuda::device_ptr<float> d_x_ptr;
  unsigned int seed;
  void *args[2];

  cpc::cuda::stream *streams;
  
  inline gpu(cpc::options &opt, cpc::cuda::device &dev):opt(&opt), dev(dev), ctx(dev),  version(dev.get_arch()), standard(cpc::cuda::get_std()){
    ctx.set_current();
    
    compile(0, 1);
    cpc::cuda::function kernel = moments.get_function("moments0");

    calc_best_grid(kernel);
    align_paths_and_points_to_grid();

    stride = blocks.x * threads.x;
    size = num_kernels * stride * sizeof(float);
    d_x = cpc::cuda::device_ptr<float>(size * 2);
    h_x = cpc::cuda::host_ptr<float>(size);
    d_x_ptr = d_x[0];

    args[0] = &d_x_ptr;
    args[1] = &seed;

    streams = new cpc::cuda::stream[num_kernels + 1];
  }

  inline ~gpu(){
    delete[] streams;
  }

  inline void compile(int start, int n){
    const std::string source = cpc::get_kernel_source(opt->d, opt->params, opt->npaths, start, n);
    const std::string ptx = cpc::compile(source, version, standard);

    moments = cpc::cuda::module(ptx);
  }

  inline void compile(int start){
    compile(start, num_kernels);
  }

  inline void run(int idx){
    for (int i = 0; i < num_kernels; ++i){
      d_x_ptr = d_x[(idx * num_kernels + i) * stride];
      seed = time(NULL);
      moments.get_function("moments" + std::to_string(i)).launch(blocks, threads, 0, streams[i], args);
    }
  }

  inline void run_synchronize(){
    ctx.synchronize();
  }

  inline void copy(int idx){
    d_x_ptr = d_x[idx * num_kernels * stride];
    d_x_ptr.to_host(h_x, size, streams[num_kernels]); 
  }

  inline void copy_synchronize(){
    streams[num_kernels].synchronize();
  }

  float sum(int idx = 0){
    return cpc::sum(&h_x[idx * stride], stride);
  }

  void align_paths_and_points_to_grid(){
    int gpu_version      = dev.get_version();
    int concurrent       = cpc::cuda::get_max_concurrent_kernels(gpu_version);
    int max_paths_kernel = blocks.x * threads.x * opt->npaths;

    num_runs = 1;
    num_kernels = 1;
    if (opt->paths >= max_paths_kernel * 0.75f){
      num_runs = (int) roundf(((float) opt->paths) / max_paths_kernel);
      opt->paths = num_runs * max_paths_kernel;
    } else {
      int l, r, ld, rd;
      l = r = 0;
      for (int div = 2; div < concurrent; ++div){
        int n = (blocks.x / div) * threads.x * opt->npaths;
        if (n >= opt->paths){
        r = n;
        rd = div;
        } else {
        l = n;
        ld = div;
        break;
        } 
      }

      if (r == 0){
        opt->paths = l;
        num_kernels = ld;
      } else if (l == 0){
        opt->paths = r;
        num_kernels = rd;
      } else {
        int m = (l + r) / 2;
        num_kernels = opt->paths > m ? rd : ld;
        opt->paths = opt->paths > m ? r : l;
      }

      blocks.x = opt->paths / (threads.x * opt->npaths);
      opt->d.set_points((opt->d.points + num_kernels - 1) / num_kernels * num_kernels);
    }
  }

  void calc_best_grid(cpc::cuda::function &kernel){
    int max_threads_block = dev.get_attribute(CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_BLOCK);     
    int warp              = dev.get_attribute(CU_DEVICE_ATTRIBUTE_WARP_SIZE);  
    int sm                = dev.get_attribute(CU_DEVICE_ATTRIBUTE_MULTIPROCESSOR_COUNT);

    int best_occupancy = 0; 
    for (int block_size = warp; block_size < max_threads_block; block_size += warp){
      int used_blocks;
      cuErrchk(cuOccupancyMaxActiveBlocksPerMultiprocessor(&used_blocks, kernel, block_size, 0));
      if (used_blocks * block_size > best_occupancy){ 
        best_occupancy = used_blocks * block_size;
        threads.x = block_size;
        blocks.x = used_blocks;
      }
    }
    blocks.x *= sm;
  }
};

} //namespace cpc

#endif //CPC_GPU_H
