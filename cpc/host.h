#ifndef CPC_HOST_H
#define CPC_HOST_H

#include <nvrtc.h>
#include <cuda.h>
#include <stdio.h>
#include <string>
#include <sstream>

#include <cpc/cuda/cuda.h>
#include <cpc/avx.h>
#include <cpc/model.h>  
#include <cpc/options.h>

std::string ftoa(float val){
    std::ostringstream s;
    s.precision(15);
    s << std::fixed << val;
    s << 'f';
    return s.str();
}

namespace cpc{

const
std::string get_kernel_source(const cpc::domain &d, const cpc::kernel &params, int npaths, int start, int n){
  std::string source = "\n\
    #define NPATHS "  + std::to_string(npaths)         + "                  \n\
    #define STEPS "   + std::to_string(params.steps)   + "                  \n\
    #define TRIGGER " + std::to_string(params.trigger) + "                  \n"
#ifdef NOUNROLL
    "#define NOUNROLL\n"
#endif
#ifdef DEFAULT
    "#define DEFAULT\n"
#endif
#ifdef NOINLINE
    "#define NOINLINE\n"
#endif
#ifdef DIVERGENCE
    "#define DIVERGENCE\n"
#endif
    "#include <cpc/defines.h>\n"; 
  for (int i = 0; i < n; ++i){
    const cpc::model m = d[start + i];
    source += "                                                             \n\
    #define DP "      + ftoa(m.dp)                     + "                  \n\
    #define LAMBDA "  + ftoa(m.lambda)                 + "                  \n\
    #define MEAN "    + ftoa(m.mean)                   + "                  \n\
    #define SPPF "    + ftoa(m.sppf)                   + "                  \n\
    #define POS "     + ftoa(m.get_pos())              + "                  \n\
    #define NEG "     + ftoa(m.get_neg())              + "                  \n\
    #define AMPMEAN " + ftoa(m.get_ampmean())          + "                  \n\
    #define IDX "     + std::to_string(i)              + "                  \n\
    #include <cpc/kernel.h>                                                 \n\
    #undef DP                                                               \n\
    #undef LAMBDA                                                           \n\
    #undef MEAN                                                             \n\
    #undef SPPF                                                             \n\
    #undef POS                                                              \n\
    #undef NEG                                                              \n\
    #undef AMPMEAN                                                          \n\
    #undef IDX\n";
  }
  return source;
}

const std::string compile(const std::string &source, const char *version, const char *standard){
  cuda::program prog(source);
  const char *options[] = {"-use_fast_math ", "-restrict", "-I.", version, standard};
  const std::string ptx = prog.compile(options, 5);
  return ptx;
}
  
float sum(float *x, int n){
  __m256 sum = _mm256_setzero_ps();
  #pragma parallel for reduction(+:sum)
  for (int i = 0; i < n; i += 8){
    sum = _mm256_add_ps(sum, _mm256_load_ps(&x[i]));
  }
  __m128 slow = _mm256_castps256_ps128(sum);
  __m128 shigh = _mm256_extractf128_ps(sum, 1);
  slow = _mm_add_ps(slow, shigh);

  __m128 shuf = _mm_movehdup_ps(slow);
  __m128 sums = _mm_add_ps(slow, shuf);
  shuf = _mm_movehl_ps(shuf, sums);
  sums = _mm_add_ss(sums, shuf);
  float s = _mm_cvtss_f32(sums);
  return s;
} 

float moment(float ax, const model &m, const kernel &params){
  return ax / (((float) params.steps) * m.get_dt());
}

} //namespace cpc

#endif //CPC_HOST_H
