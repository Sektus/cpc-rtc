#include <cpc/model.h>

extern "C" {
 
__global__ void CONCAT(moments, IDX) (float * const d_x, unsigned int seed){
  const int tidx = threadIdx.x + blockIdx.x * blockDim.x;
  const int pidx = tidx * NPATHS;

  constexpr cpc::model m(DP, LAMBDA, MEAN, SPPF, POS, NEG, AMPMEAN);

  const unsigned int lseed = seed;
  unsigned int seeds[NPATHS];
  unsigned int pcd_seed;
  float x[NPATHS], x0[NPATHS];
  int xfc = 0;

#ifndef DIVERGENCE
  pcd_seed = (lseed << 10) + tidx / 32 * 32;
#else
  pcd_seed = (lseed << 10) + tidx;
#endif
  __unroll__
  for (int path = 0; path < NPATHS; ++path){
    seeds[path] = (lseed << 10) + pidx + path;
    x[path] = 2.0f * cpc::uniform(seeds[path]) - 1.0f;
  }

  int pcd = m.get_pcd(pcd_seed);
  int step = TRIGGER;
  while (step != 0){
    pcd = min(pcd, step);
    step -= pcd;

    for (; pcd > 0; --pcd){
      __unroll__
      for (int path = 0; path < NPATHS; ++path){
        m.predcorr(x[path]);
      }
    }

    __unroll__
    for (int path = 0; path < NPATHS; ++path){
      x[path] += m.get_jump(seeds[path]);
      cpc::fold(x[path], xfc);
    }
    pcd = m.get_pcd(pcd_seed);
  }

  xfc = 0;
  __unroll__
  for (int path = 0; path < NPATHS; ++path){
    x0[path] = x[path];
  }

  step = STEPS;
  while (step != 0){
    pcd = min(pcd, step);
    step -= pcd;

    for (; pcd > 0; --pcd){
      __unroll__
      for (int path = 0; path < NPATHS; ++path){
        m.predcorr(x[path]);
      }
    }

    __unroll__
    for (int path = 0; path < NPATHS; ++path){
      x[path] += m.get_jump(seeds[path]);
      cpc::fold(x[path], xfc);
    }
    pcd = m.get_pcd(pcd_seed);
  }

  float sum = 0.0f;
  __unroll__
  for (int path = 0; path < NPATHS; ++path){
    sum += x[path] - x0[path];
  }
  sum += xfc * 2.0f / NPATHS;
  d_x[tidx] = sum;
}

}
