#ifndef CPC_MATH_H
#define CPC_MATH_H

#define PI 3.14159265358979f

#if defined(__CUDACC__) && defined(NOINLINE)
#define __cpcinline__ __noinline__
#elif defined(__CUDACC__) && defined(DEFAULT)
#define __cpcinline__
#elif defined(__CUDACC__)
#define __cpcinline__ __forceinline__
#endif

#if !defined(__CUDACC__) && defined(NOINLINE)
#define __cpcinline__ __attribute__ ((noinline))
#elif !defined(__CUDACC__) && defined(DEFAULT)
#define __cpcinline__
#elif !defined(__CUDACC__)
#define __cpcinline__ __attribute__((always_inline)) inline
#endif

#ifndef __CUDACC__
#define __host__
#define __device__
#include <math.h>
#include <cpc/avx.h>
#endif //__CUDACC__

namespace cpc {

__cpcinline__ __device__ __host__
int min(int x, int y){
  return x < y ? x: y;
}

#ifdef __CUDACC__
__cpcinline__ __device__ __host__
void 
fold(float &x, int &xfc)
{
  const int tmp = (int) (x / 2.0f);

  x -= tmp * 2.0f;
  xfc += tmp;
}
#endif //__CUDACC__

#ifndef __CUDACC__
__cpcinline__ float
logf(float x){
  union { float f; unsigned int i; } vx = { x};
  float y = vx.i;
  y *= 8.2629582881927490e-8f;
  return y - 87.989971088f;
}

__cpcinline__ __m256
logf(__m256 x)
{
  __m256 y = _mm256_fcvt_epu32_ps(_mm256_castps_si256(x));
  y = _mm256_fmsub_ps(y, _mm256_set1_ps(8.2629582881927490e-8f), _mm256_set1_ps(87.989971088f));
	return y;
}

__cpcinline__ void 
fold(__m256i &x, __m256 &xfc){
  const __m256 c  = _mm256_set1_ps(2.0f);
  const __m256 b  = _mm256_set1_ps(powf(2.0f,  22));
  const __m256 ub = _mm256_set1_ps(powf(2.0f, -22));

  __m256 xf = _mm256_mul_ps(_mm256_cvtepi32_ps(x), ub);
  __m256 n  = _mm256_floor_ps(_mm256_div_ps(xf, c)); 

  xf  = _mm256_fnmadd_ps(n, c, xf);
	x   = _mm256_cvtps_epi32(_mm256_mul_ps(xf, b));
  xfc = _mm256_add_ps(xfc, n);
} 

#endif //__CUDACC__

} //namespace cpc

#ifndef __CUDACC__

#ifdef __cplusplus
extern "C" {
#endif 
__m256  _ZGVdN8v_cosf(__m256 x);               /* _mm256_cos_ps(x)                         */
__m256  _ZGVdN8v_expf(__m256 x);               /* _mm256_exp_ps(x)                         */ 
__m256  _ZGVdN8v_logf(__m256 x);               /* _mm256_log_ps(x)                         */
__m256  _ZGVdN8v_sinf(__m256 x);               /* _mm256_sin_ps(x)                         */   
__m256  _ZGVdN8vv_powf(__m256 x, __m256 y);    /* _mm256_pow_ps(x, y)                      */
#ifdef __cplusplus
}
#endif

__cpcinline__ __m256 _mm256_cos_ps(__m256 x) {
  return _ZGVdN8v_cosf(x);
}

__cpcinline__ __m256 _mm256_exp_ps(__m256 x) {
  return _ZGVdN8v_expf(x);
}

__cpcinline__ __m256 _mm256_log_ps(__m256 x) {
  //return _ZGVdN8v_logf(x);
  return cpc::logf(x);
}

__cpcinline__ __m256 _mm256_sin_ps(__m256 x) {
  return _ZGVdN8v_sinf(x);
}

__cpcinline__ __m256 _mm256_pow_ps(__m256 x, __m256 y) {
  return _ZGVdN8vv_powf(x, y);
}

#endif //__CUDACC__

#endif //CPC_MATH_H
