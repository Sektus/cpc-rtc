#ifndef CPC_MODEL_H
#define CPC_MODEL_H

#if defined(__CUDACC__) && defined(NOINLINE)
#define __cpcinline__ __noinline__
#elif defined(__CUDACC__) && defined(DEFAULT)
#define __cpcinline__
#elif defined(__CUDACC__)
#define __cpcinline__ __forceinline__
#endif

#if !defined(__CUDACC__) && defined(NOINLINE)
#define __cpcinline__ __attribute__ ((noinline))
#elif !defined(__CUDACC__) && defined(DEFAULT)
#define __cpcinline__
#elif !defined(__CUDACC__)
#define __cpcinline__ __attribute__((always_inline)) inline
#endif

#include <cpc/random.h>
#include <cpc/math.h>

#ifndef __CUDACC__
#define __host__
#define __device__
#include <cpc/avx.h>
#include <math.h>
#endif //__CUDACC__

namespace cpc {

struct model {
  float dp;
  float lambda;
  float mean;
  float sppf;
#ifndef __CUDACC__
  __m256i pos;
  __m256i neg;
  __m256 ampmean;
  __m256 base;
#endif
#ifdef __CUDACC__
  float pos;
  float neg;
  float ampmean;
#endif
   
#ifdef __CUDACC__
  __cpcinline__ __device__
  constexpr model(float dp, float lambda, float mean, int spp, float pos, float neg, float ampmean): 
        dp(dp), lambda(lambda), mean(mean), sppf(spp), pos(pos),
        neg(neg), ampmean(ampmean){}
#endif

#ifndef __CUDACC__
  model(float dp = 1.1f, float lambda = 1.1f, float mean = 1.1f, int spp = 3): 
        dp(dp), lambda(lambda), mean(mean), sppf(spp){
    init();
  }

  void 
  init(){
    float comp = sqrtf(dp / lambda) / sppf;
    float dt = 1.0f / lambda / sppf;

    float b = powf(2.0f, 22);
    base = _mm256_set1_ps(b);

    b = powf(2.0f, 21);
    int dtf = dt * b;
    int compf = comp * b;
    pos = _mm256_set1_epi32(dtf - compf);    
    neg = _mm256_set1_epi32(-dtf - compf);
    ampmean = _mm256_set1_ps(-sqrtf(lambda / dp));
  }

  void
  set_value(float val, char domain, bool log = false){
    if (log) val = exp10f(val);

    if (domain == 'p'){
      dp = val;
      if (mean != 0.0f) lambda = mean * mean / dp;
    } else if (domain == 'l'){
      lambda = val;
      if (mean != 0.0f) dp = mean * mean / lambda;
    }
    init();
  }

  __cpcinline__
  float get_comp() const {
   return sqrtf(dp / lambda) / sppf;
  } 

  __cpcinline__
  float get_ampmean() const {
   return sqrtf(lambda / dp);
  } 

  __cpcinline__
  float get_pos() const {
    return (get_dt() - get_comp()) / 2.0f;
  } 

  __cpcinline__
  float get_neg() const{
    return (-get_dt() - get_comp()) / 2.0f;
  } 
#endif

  __cpcinline__ __device__ __host__
  float get_dt() const {
    float dt = 1.0f / lambda / sppf;
    if (dt < 0){
      return -dt;
    } else {
      return dt;
    }
  }

#ifndef __CUDACC__
  __cpcinline__
  __m256i drift(const __m256i x) const {
    __m256i mask = _mm256_srli_epi32(x, 22);
            mask = _mm256_slli_epi32(mask, 22);
            mask = _mm256_cmpeq_epi32(_mm256_set1_epi32(1 << 22), _mm256_and_si256(mask, _mm256_set1_epi32(1 << 22))); 
    return         _mm256_blendv_epi8(neg, pos, mask);
  }

  __cpcinline__
  __m256i get_jump(__m256i &seeds) const {
    __m256 res = _mm256_div_ps(_mm256_log_ps(uniform(seeds)), ampmean);
    res = _mm256_mul_ps(res, base);
    return _mm256_cvtps_epi32(res);
  }

  __cpcinline__
  void predcorr(__m256i &x) const {
    __m256i xt, xtt;

    xt = drift(x);
    xtt = drift(_mm256_add_epi32(x, _mm256_add_epi32(xt, xt)));
    xtt = drift(_mm256_add_epi32(x, _mm256_add_epi32(xt, xtt)));
    x = _mm256_add_epi32(x, _mm256_add_epi32(xt, xtt));
  }

  void * operator new[](std::size_t size){
    return aligned_alloc(alignof(model), size);
  }

  void operator delete(void *ptr){
    free(ptr);
  }

  void operator delete[](void *ptr){
    free(ptr);
  }
#endif

#ifdef __CUDACC__
  __cpcinline__ __device__ 
  float drift(const float x) const{
    if (sinf(PI * x) > 0.0f){
      return neg;
    } else {
      return pos;
    }
  }

  __cpcinline__ __device__ 
  float get_jump(unsigned int &seed) const
  {    
      return -logf(uniform(seed)) / ampmean;
  }

  __cpcinline__ __device__
  void predcorr(float &x) const {
    float xt, xtt;

    xt = drift(x);
    xtt = drift(x + xt + xt);
    xtt = drift(x + xt + xtt);
    x += xt + xtt;
  }
#endif

  __cpcinline__ __device__ __host__
  int get_pcd(unsigned int &seed) const {
    return (int) (ceilf(-logf(uniform(seed)) * sppf)) + 1;
  }
};


struct kernel {
  int trigger;
  int steps;

  __cpcinline__ __device__ __host__
  constexpr kernel(int trigger = 3, int steps = 5): trigger(trigger), steps(steps - trigger){}
}; 

} //namespace cpc

#endif //CPC_MODEL_H
