#ifndef CPC_OPTIONS_H
#define CPC_OPTIONS_H

#include <getopt.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

namespace cpc {

enum configuration { CPU, GPU, HYBRID };

struct domain {
  cpc::model m;
  char domainx;
  float beginx;
  float endx;
  float step;
  int points;
  bool logx;

  inline domain(): m(1.1f, 1.1f, 1.1f, 2.0f), domainx('p'), beginx(0.0f), 
                   endx(0.0f), step(0.0f), points(1), logx(false){}

  inline domain(cpc::model m, char domainx, float beginx, float endx, 
                int points, bool logx): m(m), domainx(domainx), beginx(beginx),
                endx(endx), points(points), logx(logx), step((endx - beginx) / points){}

  inline void set_points(int points){
    this->points = points;
    step = (endx - beginx) / points;
  }

  const cpc::model operator[](size_t idx) const{ 
    cpc::model res = m;
    res.set_value(beginx + ((float) idx) * step, domainx, logx);
    return res;
  }
};


static struct option opt[] = {
  {"Dp", required_argument, NULL, 'a'},
  {"lambda", required_argument, NULL, 'b'},
  {"mean", required_argument, NULL, 'c'},
  {"dev", required_argument, NULL, 'd'},
  {"npaths", required_argument, NULL, 'e'},
  {"paths", required_argument, NULL, 'f'},
  {"periods", required_argument, NULL, 'g'},
  {"trans", required_argument, NULL, 'h'},
  {"spp", required_argument, NULL, 'i'},
  {"mode", required_argument, NULL, 'k'},
  {"domain", required_argument, NULL, 'l'},
  {"domainx", required_argument, NULL, 'm'},
  {"logx", required_argument, NULL, 'n'},
  {"points", required_argument, NULL, 'o'},
  {"beginx", required_argument, NULL, 'p'},
  {"endx", required_argument, NULL, 'q'},
  {"cpu", required_argument, NULL, 'A'},
  {"cpu_npaths", required_argument, NULL, 'B'},
  {"conf", required_argument, NULL, 'C'},
  {0,0,0,0}
};


struct options {
  int dev;
  int paths;
  int npaths;

  int cpu_paths;
  int cpu_npaths;
  float cpu;

  cpc::domain d;
  cpc::kernel params;
  configuration conf;

  int argc;
  char **argv;
  options(int argc, char **argv): argc(argc), argv(argv) {}

  bool parse(){
    dev = 0;
    paths = 1;
    npaths = 1;

    cpu_npaths = 1;
    cpu = 0.0f;

    float dp = 0.0f;
    float lambda = 0.0f;
    float mean = 0.0f;

    char domainx = 'p';
    float beginx = 0.0f;
    float endx = 0.0f;
    int points = 1;
    bool logx = false;

    float trans = 0.0f;
    int periods = 1;
    int spp = 1;
    int trigger = 1;

    conf = configuration::HYBRID;

    int c;
    while( (c = getopt_long(argc, argv, "a:b:c:d:e:f:g:h:i:k:l:m:n:o:p:q:A:B:C", opt, NULL)) != EOF) {
      switch (c) {
        case 'a':
          dp = atof(optarg);
          break;
        case 'b':
          lambda = atof(optarg);   
          break;
        case 'c':
          mean = atof(optarg);
          break;
        case 'd':
          dev = atoi(optarg);
          break;
        case 'e':
          npaths = atoi(optarg);
          break;
        case 'f':
          paths = atoi(optarg);
          break;
        case 'g':
          periods = atoi(optarg);
          break;
        case 'h':
          trans = atof(optarg);
          break;
        case 'i':
          spp = atoi(optarg);
          break;
        case 'k':
          if (strcmp(optarg, "moments")){
              return false;
          }
          break;
        case 'l':
          break;
        case 'm':
          domainx = optarg[0]; 
          break;
        case 'n':
          logx = atoi(optarg);
          break;
        case 'o':
          points = atoi(optarg);
          break;
        case 'p':
          beginx = atof(optarg);
          break;
        case 'q':
          endx = atof(optarg);
          break;
        case 'A':
          cpu = atof(optarg);
          break;
        case 'B':
          cpu_npaths = atoi(optarg);
          break;
        case 'C':
          if (!strcmp(optarg, "cpu")){
            conf = configuration::CPU;
          } else if (!strcmp(optarg, "gpu")){
            conf = configuration::GPU;
          } else if (!strcmp(optarg, "hybrid")){
            conf = configuration::HYBRID;
          } else {
            return false;
          }
          break;
      }
    }

    trigger = trans * periods * spp;
    if (beginx > endx){
      float tmp = endx;
      endx = beginx;
      beginx = tmp;
    }

    if (conf == configuration::GPU || conf == configuration::HYBRID){
      if (npaths < 1){
        npaths = 1;
      } else if (npaths > 16){
        npaths = 16;
      }

      if (cpu_npaths < 1){
        cpu_npaths = 1;
      } else if (cpu_npaths == 3){
        cpu_npaths = 2;
      } else if (cpu_npaths > 4){
        cpu_npaths = 4;
      }
    } else {
      cpu = 1.0f;
      if (npaths < 1){
        npaths = 1;
      } else if (npaths == 3){
        npaths = 2;
      } else if (npaths > 4){
        npaths = 4;
      }
    }

    params = cpc::kernel(trigger, spp * periods);

    cpc::model m(dp, lambda, mean, spp);
    d = cpc::domain(m, domainx, beginx, endx, points, logx);
    return true;
  }

  void usage()
  {
    printf("Usage: %s <params> \n\n", argv[0]);
    printf("Model params:\n");
    printf("    -a, --dp=FLOAT          set the Poissonian noise intensity 'D_P' to FLOAT\n");
    printf("    -b, --lambda=FLOAT      set the Poissonian kicks frequency '\\lambda' to FLOAT\n");
    printf("    -c, --mean=FLOAT        if is nonzero, fix the mean value of Poissonian noise to FLOAT, matters only for domains p, l\n");
    printf("Simulation params:\n");
    printf("    -d, --dev=INT           set the gpu device to INT\n");
    printf("    -e, --npaths=INT        set number of paths generated per thread\n");
    printf("    -f, --paths=LONG        set the number of paths to LONG\n");
    printf("    -g, --periods=LONG      set the number of periods to LONG\n");
    printf("    -h, --trans=FLOAT       specify fraction FLOAT of periods which stands for transients\n");
    printf("    -i, --spp=INT           specify how many integration steps should be calculated for a characteristic time scale of Poissonian noise\n");
    printf("Output params:\n");
    printf("    -k, --mode=STRING       sets the output mode. STRING can be one of:\n");
    printf("                            moments: the first cpc::moment <<v>>\n");
    printf("    -l, --domain=STRING     simultaneously scan over one or two model params. STRING can be one of:\n");
    printf("                            1d: only one parameter\n");
    printf("    -m, --domainx=CHAR      sets the first domain of the moments. CHAR can be one of:\n");
    printf("                            p: Dp; l: lambda\n");
    printf("    -n, --logx=INT          choose between linear and logarithmic scale of the domainx\n");
    printf("                            0: linear; 1: logarithmic\n");
    printf("    -o, --points=INT        set the number of samples to generate between begin and end\n");
    printf("    -p, --beginx=FLOAT      set the starting value of the domainx to FLOAT\n");
    printf("    -q, --endx=FLOAT        set the end value of the domainx to FLOAT\n");
    printf("    -A, --cpu=FLOAT         set the ratio of work for cpu to FLOAT\n");
    printf("\n");
  }
};

}

#endif //CPC_OPTIONS_H
