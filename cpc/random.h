#ifndef CPC_RANDOM_H
#define CPC_RANDOM_H

#if defined(__CUDACC__) && defined(NOINLINE)
#define __cpcinline__ __noinline__
#elif defined(__CUDACC__) && defined(DEFAULT)
#define __cpcinline__
#elif defined(__CUDACC__)
#define __cpcinline__ __forceinline__
#endif

#if !defined(__CUDACC__) && defined(NOINLINE)
#define __cpcinline__ __attribute__ ((noinline))
#elif !defined(__CUDACC__) && defined(DEFAULT)
#define __cpcinline__
#elif !defined(__CUDACC__)
#define __cpcinline__ __attribute__((always_inline)) inline
#endif

#ifndef __CUDACC__
#define __host__
#define __device__
#include <cpc/avx.h>
#endif //__CUDACC__

namespace cpc {

__cpcinline__ __device__ __host__
float 
uniform(unsigned int &x){
  x = x ^ ( x >> 13);
  x = x ^ ( x << 17);
  x = x ^ ( x >> 5);

  return static_cast<float>(x) / 4294967296.0f;
}

#ifndef __CUDACC__
__cpcinline__ __m256
uniform(__m256i &x){
  x = _mm256_xor_si256(x, _mm256_srli_epi32(x, 13));
  x = _mm256_xor_si256(x, _mm256_slli_epi32(x, 17));
  x = _mm256_xor_si256(x, _mm256_srli_epi32(x, 5));

  return _mm256_div_ps(_mm256_fcvt_epu32_ps(x), _mm256_set1_ps(4294967296.0f));
}
#endif //__CUDACC__

} //namespace cpc

#endif //CPC_RANDOM_H
