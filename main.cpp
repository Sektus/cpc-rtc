#include <stdio.h>
#include <cpc/cpc.h>


int main(int argc, char **argv){
  cpc::options opt(argc, argv);

  if (!opt.parse()) {
    opt.usage();
    return EXIT_SUCCESS;
  }

  if (opt.conf == cpc::configuration::CPU){
    cpc::cpu c(opt);
    printf("%d %d %d\n", opt.paths, opt.d.points, opt.params.trigger + opt.params.steps);
    printf("#%c <<v>>\n", opt.d.domainx);

    for (int i = 0; i < opt.d.points; ++i){
      cpc::model m = opt.d[i];

      c.run(i, opt.npaths);

      float sv = c.sum();
      float av = sv / (c.threads * 8);
      float mv = cpc::moment(av, m, opt.params);

      float dx;
      if (opt.d.domainx == 'p') {
        dx = m.dp;
      } else if (opt.d.domainx == 'l'){
        dx = m.lambda;
      }

      printf("%e %e\n", dx, mv); 
    }
  } else if (opt.conf == cpc::configuration::GPU){
    cpc::cuda::init();
    cpc::cuda::device dev(opt.dev);
    cpc::gpu g(opt, dev);

    printf("%d %d %d\n", opt.paths, opt.d.points, opt.params.trigger + opt.params.steps);
    printf("#%c <<v>>\n", opt.d.domainx);

    int curr_idx = 0;
    int next_idx = 1;

    float sv = 0.0f;

    g.compile(0);
    g.run(curr_idx);
    for (int p = 0; p < opt.d.points; p += g.num_kernels){
      for (int run = 1; run < g.num_runs; ++run){
          g.run_synchronize();
          g.run(next_idx);

          g.copy(curr_idx);
          g.copy_synchronize();
          
          sv += g.sum();

          curr_idx = next_idx;
          next_idx = (next_idx + 1) % 2;
      }

      if (p != opt.d.points - g.num_kernels){
        g.compile(p + g.num_kernels);

        g.run_synchronize();
        g.run(next_idx);  
      } else {
        g.run_synchronize();
      }
      g.copy(curr_idx);
      g.copy_synchronize();

      for (int i = 0; i < g.num_kernels; ++i){
        const cpc::model m = opt.d[p + i];
        const cpc::kernel params = opt.params;

        sv += g.sum(i);
        float av = sv / (opt.paths / opt.npaths);
        float mv = cpc::moment(av, m, params);
        sv = 0.0f;

        float dx;
        if (opt.d.domainx == 'p') {
          dx = m.dp;
        } else if (opt.d.domainx == 'l'){
          dx = m.lambda;
        }

        printf("%e %e\n", dx, mv); 
      }

      curr_idx = next_idx;
      next_idx = (next_idx + 1) % 2;
    }
  } else {
    cpc::cuda::init();
    cpc::cuda::device dev(opt.dev);
    cpc::gpu g(opt, dev);
    cpc::cpu c(g);

    printf("%d %d %d\n", opt.paths + opt.cpu_paths, opt.d.points, opt.params.trigger + opt.params.steps);
    printf("#%c <<v>>\n", opt.d.domainx);

    int curr_idx = 0;
    int next_idx = 1;

    float sv = 0.0f;

    g.compile(0);
    g.run(curr_idx);
    c.run(curr_idx, opt.cpu_npaths, curr_idx);
    for (int p = 0; p < opt.d.points; p += g.num_kernels){
      for (int run = 1; run < g.num_runs; ++run){
          sv += c.sum(curr_idx * c.num_kernels);

          g.run_synchronize();
          g.run(next_idx);

          g.copy(curr_idx);

          c.run(p, opt.cpu_npaths, next_idx);

          curr_idx = next_idx;
          next_idx = (next_idx + 1) % 2;

          g.copy_synchronize();
          sv += g.sum();
      }

      if (p != opt.d.points - g.num_kernels){
        g.compile(p + g.num_kernels);

        g.run_synchronize();
        g.run(next_idx);  
      } else {
        g.run_synchronize();
      }

      g.copy(curr_idx);

      if (p != opt.d.points - g.num_kernels){
        c.run(p + c.num_kernels, opt.cpu_npaths, next_idx);
      }

      g.copy_synchronize();

      for (int i = 0; i < g.num_kernels; ++i){
        const cpc::model m = opt.d[p + i];
        const cpc::kernel params = opt.params;

        sv += g.sum(i);
        sv += c.sum(curr_idx * c.num_kernels + i);
        float av = sv / (opt.paths / opt.npaths + opt.cpu_paths / opt.cpu_npaths);
        float mv = cpc::moment(av, m, params);
        sv = 0.0f;

        float dx;
        if (opt.d.domainx == 'p') {
          dx = m.dp;
        } else if (opt.d.domainx == 'l'){
          dx = m.lambda;
        }

        printf("%e %e\n", dx, mv); 
      }

      curr_idx = next_idx;
      next_idx = (next_idx + 1) % 2;
    }
  }
}

