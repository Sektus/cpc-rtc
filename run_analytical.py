from pathlib import Path
from test import *

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

plt.style.use("ggplot")
colors = list(plt.rcParams['axes.prop_cycle'])
colors = [i['color'] for i in colors]

cwd = Path.cwd()
print(f"Output dir = {out_dir}")

rtc_ex = cwd / 'prog'

cpc_params = {
        #model
        "Dp":      0,
        "lambda":  0,
        "mean":    2.5,

        #simulation
        "dev":     0,
        "paths":   get_maximum_threads() * 4,
        "periods": 2048,
        "spp":     512,
        "trans":   0.10,

        #output
        "mode":    'moments',
        "points":  16,
        "beginx":  -1,
        "endx":    1,
        "domain":  '1d',
        "domainx": 'p',
        "logx":     1,
        }

rtc_params = cpc_params.copy()
rtc_params["npaths"] = 4
rtc_params["cpu"] = 0
rtc_params["cpu_npaths"] = 4
rtc_params["conf"] = "gpu"

cpc_params["block"] = 128
cpc_params["samples"] = 4096

make("best")
for mean in [1.5, 2.5]:
    rtc_params["mean"] = mean
    run(rtc_ex, out_dir / f"cpc-rtc_{mean}.csv", verbose=3, **rtc_params)

savepath = cwd / "test" / "plots"
if not savepath.exists():
    savepath.mkdir()


dp = np.linspace(0.01, 10, 1000)

lns = []
idx = 0

markers = ["", "", "o", "X"]
linestyles = ["-", "--", "", ""]


for m in [1.5, 2.5]:
    lmd = m ** 2 / dp
    
    a = np.sqrt(dp * (m ** 2 / dp))
    ap = np.exp( a / (dp * (a + 1)))
    am = np.exp( a / (dp * (a - 1)))
    
    j = (am - ap) / (4 * dp  * (ap - 1.) * (am - 1.))
    
    line = plt.plot(dp, 2 * j, color=colors[0], marker=markers[idx], linestyle=linestyles[idx], label=f"a = {m}")
    plt.xlim([1e-1, 10]) 
    plt.xscale("log")
    plt.xticks([0.1, 1, 10], [0.1, 1, 10])
    
    plt.ylim([1e-3, 1])
    plt.yscale("log")
    plt.yticks([1e-3, 1e-2, 1e-1, 1e-0], ["$10^{-3}$", "$10^{-2}$", "$10^{-1}$", "$10^{0}$"])
    
    lns += line
    idx += 1
    
for m in [1.5, 2.5]: 
    df = pd.read_csv(out_dir / f"cpc-rtc_{m}.csv", skiprows=2, sep=" ")
    line = plt.plot(df["#p"], df["<<v>>"], color=colors[1], marker=markers[idx], linestyle=linestyles[idx], label=f"simulation a = {m}")
    
    lns += line
    
    plt.xlabel("$D_p$")
    plt.ylabel("$\\langle v \\rangle$", rotation="horizontal")
    idx += 1

labels = [i.get_label() for i in lns]

plt.legend(lns, labels)
plt.savefig(savepath / f"analytical.png", bbox_inches="tight", dpi=300)