import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from pathlib import Path

data = Path.cwd() / "test"

savepath = data / "plots"
if not savepath.exists():
    savepath.mkdir()

plt.style.use("ggplot")

turing_cores = 4352
sektus_cores = 512
gpu4_cores   = 2880

turing = pd.read_csv(data / "turing_speed.csv")
sektus = pd.read_csv(data / "sektus_speed.csv")
gpu4   = pd.read_csv(data / "gpu4_speed.csv")

cmap = plt.get_cmap("tab20")
colors = cmap(np.linspace(0, 1, len(gpu4)))

turing = turing.sort_values("sps", ascending=False)
sektus = sektus.sort_values("sps", ascending=False)
gpu4   = gpu4.sort_values("sps", ascending=False)

turing["sps"] = turing["sps"] / turing_cores
sektus["sps"] = sektus["sps"] / sektus_cores
gpu4["sps"] = gpu4["sps"] / gpu4_cores

scale = max(gpu4["sps"])

turing["sps"] = (turing["sps"] / scale * 100).astype(np.int32)
sektus["sps"] = (sektus["sps"] / scale * 100).astype(np.int32)
gpu4["sps"] = (gpu4["sps"] / scale * 100).astype(np.int32)

turing.index = range(len(turing.index))
sektus.index = range(len(sektus.index))
gpu4.index = range(len(gpu4.index))

space = 2
nbars = len(sektus)
indices = np.arange(nbars + 2 * space)

names = []
bars = []

fig = plt.figure(figsize=[6.4 * 1.5, 4.8 * 1.5])
for idx, row in sektus.iterrows():
    prog = row["prog"]
    sektus_idx = idx
    turing_idx = turing.loc[turing["prog"] == prog].index[0]
    gpu4_idx = gpu4.loc[gpu4["prog"] == prog].index[0]
    
    ind = [gpu4_idx, nbars + space + sektus_idx, 2*(nbars + space) + turing_idx]
    values = [gpu4.loc[gpu4_idx]["sps"], row["sps"], turing.loc[turing_idx]["sps"]]
    
    b = plt.bar(ind, values, color=colors[idx])
    names.append(prog)
    bars.append(b[0])
    
# plt.title("Speed (%) scaled by number of cores and best performance of Kepler architecture")
plt.legend(bars, names, bbox_to_anchor=(1.05, 1), loc='upper left')
plt.xlabel("Architecture")
plt.ylabel("Speed (%)")
plt.xticks([nbars /2, nbars + space + nbars / 2, 2 * (nbars + space) + nbars /2], ["Kepler", "Maxwell", "Turing"])
plt.savefig(savepath / "speed.png", bbox_inches = "tight", dpi=300)
plt.close()

colors = list(plt.rcParams['axes.prop_cycle'])
colors = [i['color'] for i in colors]

progs = ["gpu4.smcebi.us.edu.pl", "sektus", "turing"]
params = ["npaths"]
arch = {
    "gpu4.smcebi.us.edu.pl": "kepler",
    "sektus" : "maxwell",
    "turing" : "turing"
}

name = "cpu"
for i, prog in enumerate(progs):
    path = data / prog / f"cpc-rtc_{name}" / f"cpc-rtc_{name}.csv"
    result = pd.read_csv(path)

    x = result[name]
    y = result["time"]
    
    fig, ax1 = plt.subplots()

    ax1.set_xlabel(name)
    ax1.set_ylabel('time (s)')
    line0 = ax1.plot(x, y, color=colors[0], marker='o', linestyle='-', markevery=2, label="time(s)")

    y = result["sps"]
    ax2 = ax1.twinx()
    ax2.grid(False)
    ax2.set_ylabel('steps/s')
    line1 = ax2.plot(x, y, color=colors[1], marker='o', linestyle='-', markevery=2, label="steps/s")

    if i == 0:
        lns = line0 + line1
        labels = [l.get_label() for l in lns]
    elif i == 2:
        plt.legend(lns, labels, bbox_to_anchor=(1.1, 1), loc='upper left')

    fig.savefig(savepath / f"{arch[prog].lower()}_{name}.png", bbox_inches="tight", dpi=300)
    # plt.show()
    plt.close()

name = "periods"
for i, prog in enumerate(progs):
    path = data / prog / f"cpc_{name}" / f"cpc_{name}.csv"
    df_cpc = pd.read_csv(path)
    
    path = data / prog / f"cpc-rtc_{name}" / f"cpc-rtc_{name}.csv"
    df_rtc = pd.read_csv(path)
    
    values = df_cpc[name]
    if name == "periods":
        values = np.log2(values)

    fig, ax1 = plt.subplots()

    ax1.set_xlabel(name)
    ax1.set_ylabel('time (s)')
    line0 = ax1.plot(values, df_cpc["time"], color=colors[0], marker='o', linestyle='-', label='cpc time(s)')
    line1 = ax1.plot(values, df_rtc["time"], color=colors[1], marker='o', linestyle='-', label='rtc time(s)')

    ax2 = ax1.twinx()

    ax2.grid(False)
    ax2.set_ylabel('steps/s')
    line2 = ax2.plot(values, df_cpc["sps"], color=colors[2], marker='o', linestyle='-', label='cpc steps/s')
    line3 = ax2.plot(values, df_rtc["sps"], color=colors[3], marker='o', linestyle='-', label='rtc steps/s')

    if name == "periods":
        plt.xticks(values, [r"$2^{%d}$"%i for i in values])
    
    if i == 2:
        lns = line0 + line1 + line2 + line3
        labels = [l.get_label() for l in lns]
        plt.legend(lns, labels, bbox_to_anchor=(1.1, 1), loc='upper left')
    # plt.title(arch[prog])
    fig.savefig(savepath / f'{arch[prog].lower()}_{name}.png', bbox_inches="tight", dpi=300)
    # plt.show()
    plt.close()

npaths = {}
pname = 'npaths'
for i, prog in enumerate(progs):
    path = data / prog / f"cpc-rtc_{pname}" / f"cpc-rtc_{pname}.csv"
    result = pd.read_csv(path)
    npaths[prog] = result

for v in npaths.values():
    v.sort_values("sps", ascending = True, inplace=True)
    v["time"] *= (v["adjusted_paths"].min() * v["adjusted_points"].min()) / (v["adjusted_paths"] * v["adjusted_points"])
    v.index = range(len(v.index))

for i, prog in enumerate(npaths.keys()):
    plt.xlabel("npaths")
    plt.ylabel("steps/s")
    
    df = npaths[prog].sort_values("npaths")
    df.index = range(len(df.index))
    
    sps_min = npaths[prog]["sps"].min()
    sps_max = npaths[prog]["sps"].max()
    sps_min -= 0.2 * (sps_max - sps_min)

    line0 = plt.bar(df.index, df["sps"] - sps_min, bottom = sps_min, color=colors[0], label="steps/s")

    line1 = plt.plot(range(len(npaths[prog]["sps"])), npaths[prog]["sps"], marker="o", markersize=15, color=colors[1], label="sorted steps/s")
    for idx, val in enumerate(npaths[prog]["sps"]):
        marker = f"${npaths[prog]['npaths'][idx]}$"
        plt.plot([idx], [val], marker=marker, markersize=10, linestyle="", color='w')
        
    if i == 2:
        lns = [line0, line1[0]]
        labels = [i.get_label() for i in lns]
        plt.legend(lns, labels, bbox_to_anchor=(1.05, 1), loc='upper left')
        
    
    plt.xticks(range(len(npaths[prog])), df["npaths"])
    plt.savefig(savepath / f"{arch[prog]}_npaths.png", bbox_inches = "tight", dpi=300)
    # plt.show()
    plt.close()
