#!/usr/bin/env python3
from test import *
import numpy as np
from pathlib import Path

cwd = Path.cwd()
print(f"Output dir = {out_dir}")

rtc_ex = cwd / 'prog'
cpc_ex = cwd.parent / 'cpc' / 'poisson'

cpc = True
if not cpc_ex.exists():
    print("[WARNING] Pull cpc code from `https://github.com/jspiechowicz/cpc` to parent dir")
    cpc = False

cpc_params = {
        #model
        "Dp":      0,
        "lambda":  0,
        "mean":    2.5,

        #simulation
        "dev":     0,
        "paths":   get_maximum_threads() * 4,
        "periods": 2048,
        "spp":     512,
        "trans":   0.125,

        #output
        "mode":    'moments',
        "points":  16,
        "beginx":  -1,
        "endx":    1,
        "domain":  '1d',
        "domainx": 'p',
        "logx":     1,
        }

rtc_params = cpc_params.copy()
rtc_params["npaths"] = 4
rtc_params["cpu"] = 0
rtc_params["cpu_npaths"] = 4
rtc_params["conf"] = "gpu"

cpc_params["block"] = 128
cpc_params["samples"] = 4096
    

make("best")
df_rtc = test_param(rtc_ex, out_dir, "periods", [2 ** i for i in range(4, 17)], 5, 2, True, **rtc_params)
df_cpc = test_param(cpc_ex, out_dir, "periods", [2 ** i for i in range(4, 17)], 5, 2, True, **cpc_params)
plot(df_cpc, df_rtc, out_dir, "periods")
 
rtc_params["periods"] = max_with_eps(df_rtc, "periods", 1.025)
print(f"Best rtc periods = {rtc_params['periods']}")

cpc_params["periods"] = max_with_eps(df_cpc, "periods", 1.025)
print(f"Best cpc periods = {cpc_params['periods']}")


rtc_params["npaths"] = 1
test_speed(rtc_ex, out_dir, "npaths_1", n=5, verbose=2, **rtc_params)
rtc_params["npaths"] = 2
test_speed(rtc_ex, out_dir, "npaths_2", n=5, verbose=2, **rtc_params)


df_rtc = test_param(rtc_ex, out_dir, "npaths", [i for i in range(1, 17)], 5, 2, True, **rtc_params)

rtc_params["npaths"] = max_with_eps(df_rtc, "npaths", 1.025)
print(f"Best rtc npaths = {rtc_params['npaths']}")

paths = rtc_params['npaths'] * get_maximum_threads()
rtc_params["paths"] = paths
cpc_params["paths"] = paths
print(f"Setting up paths to {paths}")


test_speed(cpc_ex, out_dir, n=5, verbose=2, **cpc_params)
test_speed(rtc_ex, out_dir, n=5, verbose=2, **rtc_params)


rtc_params["conf"] = "hybrid"
df_rtc = test_param(rtc_ex, out_dir, "cpu", np.linspace(0, 0.6, 30), 5, 2, True, **rtc_params)
rtc_params["cpu"] = max_with_eps(df_rtc, "cpu", 1.0)

test_speed(rtc_ex, out_dir, "hybrid", n=5, verbose=2, **rtc_params)


rtc_params["conf"] = "gpu"
programs = ["default", "noinline", "nounroll", "divergence", "noinline_nounroll", "noinline_divergence", "nounroll_divergence", "noinline_nounroll_divergence"]
for prog in programs:
    make(prog)
    test_speed(rtc_ex, out_dir, prog, n=5, verbose=2, **rtc_params)
