import subprocess
from pathlib import Path
from .utils import *
import time
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
plt.style.use('ggplot')


colors = list(plt.rcParams['axes.prop_cycle'])
colors = [i['color'] for i in colors]


def run(ex, out, verbose=0, rm=False, plot=False, **params):
    if not isinstance(ex, Path):
        ex = Path(ex)
        
    with working_directory(ex.parent):
        _cmd = get_cmd(ex.name, out, **params)

        if verbose >= 1:
            print(_cmd)

        with open(out, 'w') as output:
            output.write(_cmd + "\n")

        start = time.time()
        cmd = subprocess.getoutput(_cmd)
        end = time.time()
        elapsed = end - start

        with open(out, 'r') as file:
            for _ in range(2):
                line = file.readline()
            df = pd.read_csv(file, sep = ' ')
            paths, points, steps = [int(i) for i in line.split(' ')]

        sps = get_sps(paths, points, steps, elapsed)

        if verbose >= 2:
            print(f'paths = {paths}, points = {points}, steps = {steps}, steps/s = {int(sps)}, elapsed = {round(elapsed,2)}')

        if rm:
            os.system(f'rm {out}') 

        for i in df.columns:
            pd.to_numeric(df[i])

        if verbose >= 3:
            print(df)

        if plot:
            plt.xlabel(params["domainx"])
            plt.ylabel('<<v>>')
            plt.plot(df[f'#{params["domainx"]}'], df['<<v>>'])
            plt.savefig(out.parent / (out.name.split('.')[0] + '.png'))
            plt.close()
        return df, sps, paths, points, steps, elapsed


def run_avg(ex, out_dir, verbose = 0, n = 5, **params):
    avg_time = 0
    avg_sps = 0
    mkdir(out_dir)
    
    for i in range(n):
        if verbose >= 1:
            print(f'[Run {i + 1} of {n}] ', end='')
            
        df, sps, paths, points, steps, elapsed = run(ex, out_dir / f"moments_run_{i}.csv", verbose=verbose, rm=False, plot=True, **params)
            
        avg_time += elapsed
        avg_sps += sps
    
    avg_time /= n
    avg_sps /= n

    if verbose >= 1:
            print(f'[Average] steps/s {int(avg_sps)} time {round(avg_time,2)}')
    return avg_sps, paths, points, steps, avg_time


def test_param(ex, out_dir, pname=None, values=None, n=5, verbose=0, plot=False, **params):
    if not isinstance(ex, Path):
        ex = Path(ex)
    
    if not isinstance(out_dir, Path):
        out_dir = Path(out_dir)
    
    out_dir = out_dir / f"{ex.parent.name}_{pname}"
    mkdir(out_dir)
        
    args = params.copy()
    result = {pname: values, "sps": [], "adjusted_paths":[], "adjusted_points": [], "steps": [], "time": []}
    
    if verbose >= 1:
        print(f'[Average of {n} runs]')
        
    for vidx, value in enumerate(values):
        args[pname] = value
        
        if verbose >= 1:
            print(f'[Parameter {vidx + 1} of {len(values)}] {pname} = {args[pname]}')
        
        avg_sps, paths, points, steps, avg_time = run_avg(ex, out_dir / f"{value}", verbose, n, **args)
            
        result["sps"].append(avg_sps)
        result["adjusted_paths"].append(paths)
        result["adjusted_points"].append(points)
        result["steps"].append(steps)
        result["time"].append(avg_time)
        
    result = pd.DataFrame(result)
    result.to_csv(out_dir / f"{ex.parent.name}_{pname}.csv", sep=',', index=False)
        
    if plot:
        if pname == "periods":
            values = np.log2(values)

        fig, ax1 = plt.subplots()

        ax1.set_xlabel(pname)
        ax1.set_ylabel('time (s)')
        line1 = ax1.plot(values, result["time"], color=colors[0], label='time')

        ax2 = ax1.twinx()
        
        ax2.grid(False)
        ax2.set_ylabel('steps/s')
        line2 = ax2.plot(values, result["sps"], color=colors[1], label='steps/s')

        if pname == "periods":
            plt.xticks(values, [r"$2^{%d}$"%i for i in values])

        lns = line1 + line2
        labels = [l.get_label() for l in lns]
        plt.legend(lns, labels, bbox_to_anchor=(1.1, 1), loc='upper left')
        fig.savefig(out_dir / f"{ex.parent.name}_{pname}.png", bbox_inches="tight")
        plt.close()   
    return result


def test_speed(ex, out_dir, name='', n=5, verbose=0, **params):
    if not isinstance(ex, Path):
        ex = Path(ex)
    
    if not isinstance(out_dir, Path):
        out_dir = Path(out_dir)
    mkdir(out_dir)

    csv = out_dir / f"{get_computer_name()}_speed.csv" 
    if name:
        out_dir = out_dir / f"{ex.parent.name}_{name}"
    else:
        name = ex.parent.name
        out_dir = out_dir / f"{ex.parent.name}"
    mkdir(out_dir)

    avg_sps, paths, points, steps, avg_time = run_avg(ex, out_dir, verbose, n, **params)

    result = {"sps": [avg_sps], "adjusted_paths": [paths], "adjusted_points": [points], "steps": [steps], "time": [avg_time]}
    result["prog"] = name
    result = pd.DataFrame(result)
    if csv.exists():
        df = pd.read_csv(csv)
        result = pd.concat([df, result], ignore_index=True)
    result.to_csv(csv, sep=',', index=False)


def plot(df_cpc, df_rtc, out_dir, name):
    values = df_cpc[name]
    if name == "periods":
        values = np.log2(values)

    fig, ax1 = plt.subplots()

    ax1.set_xlabel(name)
    ax1.set_ylabel('time (s)')
    line1 = ax1.plot(values, df_cpc["time"], color=colors[0], label='cpc time')
    line2 = ax1.plot(values, df_rtc["time"], color=colors[1], label='rtc time')

    ax2 = ax1.twinx()

    ax2.grid(False)
    ax2.set_ylabel('steps/s')
    line3 = ax2.plot(values, df_cpc["sps"], color=colors[2], label='cpc steps/s')
    line4 = ax2.plot(values, df_rtc["sps"], color=colors[3], label='rtc steps/s')

    if name == "periods":
        plt.xticks(values, [r"$2^{%d}$"%i for i in values])
    
    lns = line1 + line2 + line3 + line4
    labels = [l.get_label() for l in lns]
    plt.legend(lns, labels, bbox_to_anchor=(1.1, 1), loc='upper left')
    fig.savefig(out_dir / f'{name}.png', bbox_inches="tight")
    plt.close()
