#!/usr/bin/env python3
import contextlib
from pathlib import Path
import os
import socket
import subprocess
import re


@contextlib.contextmanager
def working_directory(path):
    """Changes working directory and returns to previous on exit."""
    prev_cwd = Path.cwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(prev_cwd)


def mkdir(path):
    if not path.exists():
        path.mkdir()


def get_cmd(ex, out='', **params):
    cmd = './' + ex
    for k, v in params.items():
        cmd += " --" + k + "=" + str(v)
    if (out):
        cmd += " >> " + str(out) 
    return cmd


def get_sps(paths, points, steps, t):
    return paths * points * steps / t


def get_computer_name():
    return socket.gethostname()


def make(name=''):
    cmd = f"make {name}"
    print(cmd)
    output = subprocess.getoutput(cmd)
    print(output)


def get_device_query():
    make("device_query")
    return subprocess.getoutput("./device_query")


def get_maximum_threads():
    info = get_device_query()
    
    m = re.search(r"\(\s*(\d+)\) Multiprocessors", info)
    sm = int(m.groups()[0])
    
    m = re.search(r"Maximum number of threads per multiprocessor:\s*(\d+)", info)
    threads = int(m.groups()[0])
    return sm * threads


def max_with_eps(df, name, eps=1.05):
    sps = []
    val = []
    best_sps = 0
    for idx, row in df.iterrows():
        if row["sps"] > best_sps:
            best_sps = row["sps"]
            sps.append(best_sps)
            val.append(row[name])
    
    best_sps = 0
    for s, v in zip(sps, val):
        if s > best_sps * eps:
            best_sps = s
        else:
            return v
    return v


out_dir = Path(__file__).resolve().parent / get_computer_name()
mkdir(out_dir)
